Since Nebulous CMS is a small Node.js server, there are many different ways to host it. These can vary from your own VM
instance on a public cloud, to a PaaS offering, a 'edit in the browser' hosting, to just putting it on your Raspberry
Pi in your lounge.

## Hosting ##

The following PaaS services host Glitch apps with ease, and we love all of them:

* [Glitch](https://glitch.com/)
* [Heroku](https://heroku.com/)
* [Zeit](https://zeit.co/now)

(Ends)
