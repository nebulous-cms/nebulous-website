On the [Nebulous Server](/server) page, we showed you a fairly long winded way of creating an empty Nebulous site, but
there is a better way - the Nebulous CLI.

The CLI can help you in various situations when managing a Nebulous CLI, the main two being initialising/creating a new
site and the other creating/managing content.

## Create a new Site ##

Before we get on to managing content, we'll show you an example site creation. Firstly, we'll install the
`nebulous-cli` tool:

```npm install -g nebulous-cli
```

In the directory where you want your new site, create a new one:

```$ nebulous site:create
```

At this point you'll be asked some question which will set up exactly what you need. For now just answer the first
question - the site name which is required - and we'll take the defaults to the remaining questions (just press
`<RET>`).

Next, `cd` into the new dir and start the server:

```$ cd your-new-site
$ npm start
```

Finally, open http://localhost:3000 in your browser to see your fresh new site!
