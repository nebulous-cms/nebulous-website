## 500 - Internal Server Error ##

We're ever so sorry about this. Obviously we've done something wrong and it's something we'd like to fix.

Feel free to send us a bug report on the [Nebulous Website](https://gitlab.com/nebulous-cms/nebulous-website/) repo and
we'll take a look.

Oh, and please ping us [@NebulousCMS](https://twitter.com/NebulousCMS) if this keeps happening. :) Thanks.
