Nebulous Core is the core library that reads in your JSON and Markdown files and presents a (tree) data structure of
your site to the Nebulous Server.

It is fairly straighforward in that it just reads a dir of files, however whilst this package is the organisational
brains, the plugin system (and the plugins) are the creative brains.

(Note: for this page we won't go into how plugins hook into Nebulous Core.)

## Lifecycle ##

The core library walks the tree of files in your `content/` directory, and returns a tree structure once finished. It
does this by looking for files of a certain type - specifically JSON files - and performing further actions based on
the name or contents of each.

Here's a brief view of this process, assuming the data structure returned is called `site`:

1. Find all dirs and JSON files in `content/`
  1. If `_config.json` save data as `site.config`
  1. If dir, save placeholder structure in `site.content[...]`, recursing down as needed
  1. If non-JSON file, ignore (for now)
  1. Read JSON file and store under `site.content[...]` in the right place
  1. Process any other files as specified (see below)

Once we have found a JSON file and placed it in the right place in the `site` data structure, we'll need to take
further action depending on the `"type"` specified.

In Nebulous Core, we only process two types: `page`, and `redirect`.

For `page` (which is the default if `"type"` is missing, we'll read the content from the `<page-name>.md` file, place
the content into `site.content[...][<page-name>].markup`, and the processed HTML into `site.content[...][<page-name>].html`.

For `redirect`, no further file is read, but the `"location"` in the file is stored with the content so Nebulous Server
knows where to redirect the request to.

## Examples ##

This is all rather abstract for the moment, so let's take a look at some examples. These are taken from the
[unit tests](https://gitlab.com/nebulous-cms/nebulous-core/tree/master/test) in Nebulous Core if you'd like to look around there too.

### The Empty Site (an empty `content/` dir) ###

In our first example, we'll show you the `site` data structure returned from an empty `content/` dir:

```
{
  url: '/',
  config: {},
  type: 'dir',
  meta: {},
  content: {},
  errors: [],
}
```

As you can see, there are 6 properties, and:

* the url is '/' since it is the top level
* the type is 'dir' since we are reading the top-level `content/` dir
* no `_config.json` file, hence no config
* no `_meta.json` in the dir, hence no meta information
* no other files exist, hence no content
* nothing went wrong therefore no errors

You can probably tell where information for the `_config.json` and top-level `_meta.json` data go, and these are
presented exactly as found with no manipulation performed.

Now let's take a look at a simple site with just one page. In this case the page name could be anything, but we'll use
the name `index.json` and `index.md` for the reason that the Nebulous Server looks for the `index` name in the content
if a dir was requested.

Take the files as:

```
index.json:
{
  "title": "Welcome"
}

index.md:
Hello, World!
```

We'll end up with the following data structure (`config` and `meta` omitted):

```
{
  url: '/',
  config: { ... },
  type: 'dir',
  meta: { ... },
  content: {
    index: {
      name: 'index',
      type: 'page',
      markup: 'markdown',
      meta: {
        title: 'Homepage',
      },
      content: 'Hello, World!\n',
      html: '<p>Hello, World!</p>\n',
    },
  },
  errors: [],
}
```

You can see inside the `site.content` a property called `index` which directly represents the `index.json` and
`index.md` files that exist in `content/`.

You can also see that `"type"` has a default of `"page"` and for pages the `"markup"` is defaulted to
`"markdown"`. Finally, the content and html come from the `index.md` as well as the markdown HTML generated.

## Deeper Structures ##

So let's take a look at a deeper structure which has an index file at the top level, and another index file under a
`/recipe/` dir:

```
{
  url: '/',
  config: {},
  type: 'dir',
  meta: {},
  content: {
    index: {
      type: 'page',
      name: 'index',
      markup: 'markdown',
      meta: {
        title: 'Two Levels',
      },
      content: 'Hello, World!\n',
      html: '<p>Hello, World!</p>\n',
    },
    recipe: {
      url: '/recipe/',
      type: 'dir',
      meta: {},
      content: {
        index: {
          type: 'page',
          name: 'index',
          markup: 'markdown',
          meta: {
            title: "Recipes",
          },
          content: '* Blue Cheese Surprise\n',
          html: '<ul>\n<li>Blue Cheese Surprise</li>\n</ul>\n',
        },
      },
    },
  },
  errors: [],
}
```

As you can see, the `site.content.index` appears as it did in the previous example. But this time there is also a
`recipe` property under the top level

## Plugins ##

Shortly we'll be allowing plugins to be loaded into Nebulous Core which will allow hooks into the lifecycle at specific
points. These will allow loading other files based on the `"type"` in each JSON file found.
