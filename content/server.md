Let's take you through creating your first website manually, and after that we can take a look at the Nebulous CLI and
create one more easily.

We're going to assume you have the `nebulous-server` program in your `$PATH` somehow. Perhaps you followed the
[Tutorial](/tutorial) and have it globally installed - which is perfect - otherwise you'll have to install it
somehow. Take a look at the [Installation](/installation) page for other ways of getting `nebulous-server`.

## Website Layout ##

A website is just three dirs named:

* `views/` - your PugJS views
* `static/` - your static/public files
* `content/` - your Markdown and JSON files

So let's create a new website and serve it on `localhost:3000`.

```$ mkdir app
$ cd app
$ mkdir views static content
$ echo '{"scripts":{"start":"nebulous-server"}}' > package.json
$ npm install nebulous-server
$ echo 'Welcome.' > content/index.md
$ echo '{"title":"Hello, World!"}' > content/index.json
$ echo 'div !{page.html}' > views/page.pug
```

All you need to do is make sure a `static/favicon.ico` exists too!

```$ npm start
level=info ts=1526520464699 evt=nebulous
level=info ts=1526520465736 port=3000 evt=server-started
```

And in another terminal:

```$ curl localhost:3000
<div><p>Welcome.</p></div>
```

You just made your first Nebulous CMS website.

## Server Args ##

You can customise the above options by passing command line args to the program.

```
$ nebulous-server --help
nebulous-server.js [args]

Options:
  --version          Show version number                               [boolean]
  --static-dir, -s   the dir of static files to serve prior to dynamic routes
                                         [string] [required] [default: "static"]
  --views-dir, -v    the dir use for your page templates
                                          [string] [required] [default: "views"]
  --content-dir, -c  the content dir to read on startup
                                        [string] [required] [default: "content"]
  --port, -p         the port to listen on (localhost)
                                             [string] [required] [default: 3000]
  --config, -C       Path to JSON config file                           [string]
  --help             Show help                                         [boolean]
```

You can also specific a config file (in JSON format of course) so you can store those options in your repository. Just
pass the `--config` (or `-c`) option to a filename. The following example changes the port to 8080, rather than 3000:

```
$ cat config.json
{
  "port": "8080"
}

$ nebulous-server --config config.json
...etc...
level=info ts=1528236903080 port=8080 evt=server-started
```

## Server API ##

ToDo.

(Ends)
