## Using or Installing a Theme ##

To use or install a theme, you can either set it at website creation, or at some point afterwards.

During website creation you can use the following to create a new project with a particular theme, in this example the
`nebulous-themes/base` theme:

```
$ nebulous create --theme=gitlab.com/nebulous-themes/base
```

Once you've started your website and you wish to switch themes to `nebulous-themes/skeleton`, you can run:

```
$ nebulous theme:use --theme=gitlab.com/nebulous-themes/skeleton
```

See the documentation For the [Nebulous CLI](/cli) tool for further information.

## Creating a Theme ##

Creating a theme is much like creating a website in Nebulous. You'll initialise the package (or use your own base as a
started), add some static files, create some views, and add some example content to test out your theme.

However the main differences when someone installs your theme is:

1. the content will be removed except a basic `_config.json` and `_meta.json` file
2. only the `static/` and `views/` dirs are copied
3. all other files are ignored

## Your First Theme ##

Let's start as we normally would with a new directory and a few commands for setup.

Note: these instructions are overly verbose for clarity. If you're comfortable already, you can just run `nebulous init
--theme=base` using the [nebulous-cli](https://www.npmjs.com/package/nebulous-cli) project and you'll get the same
result.

```
$ mkdir my-theme
$ cd my-theme
$ npm init
This utility will walk you through creating a package.json file.
It only covers the most common items, and tries to guess sensible defaults.

See `npm help json` for definitive documentation on these fields
and exactly what they do.

Use `npm install <pkg>` afterwards to install a package and
save it as a dependency in the package.json file.

Press ^C at any time to quit.
package name: (base) base
version: (1.0.0) 1.0.0
description: A base theme for Nebulous CMS.
entry point: (index.js) none
test command:
git repository: https://gitlab.com/nebulous-themes/base.git
keywords: nebulous-theme, nebulous-cms, nebulous
author: Andrew Chilton
license: (ISC) MIT
About to write to /home/chilts/src/nebulous-themes/base/package.json:

{
  "name": "base",
  "version": "1.0.0",
  "description": "A base theme for Nebulous CMS.",
  "main": "none",
  "scripts": {
    "test": "echo \"Error: no test specified\" && exit 1"
  },
  "repository": {
    "type": "git",
    "url": "https://gitlab.com/nebulous-themes/base.git"
  },
  "keywords": [
    "nebulous-theme",
    "nebulous-cms",
    "nebulous"
  ],
  "author": "Andrew Chilton",
  "license": "MIT",
  "bugs": {
    "url": "https://gitlab.com/nebulous-themes/base/issues"
  },
  "homepage": "https://gitlab.com/nebulous-themes/base#README"
}


Is this ok? (yes) yes
```

There are a few things to tidy up here.

1. remove the `main` element
1. remove the `scripts.test` element
1. add a `scripts.start` with the value `"nebulous-server"`

And install `nebulous-server` for testing:

```
$ npm install nebulous-server
```

The first step in complete.

## Step 2 : Create Structure ##

Now we need to set up a few things so that Nebulous knows this is a website it can serve. We'll create some dir and
grab the Nebulous CMS favicon:

```
$ mkdir content views static
$ cd static
$ wget -q https://gitlab.com/nebulous-cms/nebulous-website/raw/master/static/favicon.ico
$ cd -
```

Create some very basic `views/` and edit `layout.pug` and `page.pug` to be as shown:

```
$ cd views
```

Here's `layout.pug`:

```
$ cat layout.pug
doctype html
html(lang='en')
  head
    meta(charset='utf-8')
    meta(name='viewport', content='width=device-width, initial-scale=1, shrink-to-fit=no')
    meta(name='description', content='')
    meta(name='author', content='')
    link(rel='icon', href='/favicon.ico')
    title #{page.meta.title} | #{site.config.apex}

  body

    h1= page.meta.title
    block content
```

And `page.pug`:

```
$ cat page.pug
extends layout.pug

block content
  div !{page.html}
```

You're now finished creating views:

```
$ cd -
```

## Skeleton Content ##

You'll need to create four files here:

```
$ cd content
$ echo '{"apex": "example.com"}' > _config.json
$ echo '{"title": "My Theme"}' > _meta.json
$ echo '{"title": "Welcome"}' > index.json
$ echo 'Hello, World!' > index.md
```

Strictly speaking, these files aren't needed for a theme, but they will help you develop on locally.

## Starting the Server ##

And finally start the server and then browse to `http://localhost:3000` to view your creation:

```
$ npm start
level=info ts=1527506478407 port=3000 evt=server-started
```

Once you've finished your theme, you could publish it somewhere like GitHub or GitLab and the end user can use install it using `nebulous-cli`:

```
$ nebulous create --theme=gitlab.com/nebulous-themes/base
```

(Ends)

