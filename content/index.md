Nebulous CMS is a **fluid website** generator/server which builds from plain JSON and Markdown files. It is structured
in the same way as your website works and therefore lends itself well to both small and large sites.

It is fast and flexible and can be tailored to your needs via a plugin system, however the core is also small and
simple which means it is relatively easy to learn.

## What does a "fluid website" mean? ##

Traditionally websites have been put into two categories:

1. dynamic websites pull content from a database or backend store, build the page, and send it to the user
2. static websites build all pages up-front and serve each pre-built page upon request

Both of these cases have drawbacks of various descriptions. For example, when moving your dynamic website from one host
to another, you either have to migrate the database too or make sure the new install can still talk to the old
database. Static websites don't have that problem, but something as simple as being able to create a redirect can be
quite problematic and hard to figure out.

To alleviate both of these problems, Nebulous CMS was written to be a fluid website such that it doesn't require a
backing store and instead carries it's content around with it (thanks to the power of your version control system). Nor
does it require a separate web server such as Apache2 or Nginx to serve the dynamic parts since it is itself a
webserver written in Node.js.

When you read about 'fluid' websites, think of them as semi-static or semi-dynamic - in fact, about half way between
the two is about right.

## Fluid Websites ##

Fluid websites are a number of things. As we've noted above they're a cross between a static site generator and a fully
dynamic webserver, however there are some other properties which make them really nice.

They:

1. are easily hosted on PaaS such as Heroku, now.sh, and Glitch, or a cloud such as Google, AWS, or Azure
2. easily moved from one hosting provider to another due to the content being part of the deploy
3. don't require much configuration for the webserver, mainly just the apex and port are required
4. instantly deployed with not much more than a `git push` to your PaaS
5. can deal with dynamic actions such as form submissions, without having to outsource to another provider or having to
   load up extra code in the client

## Is my git repo code, content, prsentation, or something else? ##

Mostly your repo is content plus presentation. Almost all code is taken care of in [nebulous-server](/server) apart
from any you might need in your views or plugins.

In practice this means that you might change a lot in your `views/` directory when creating your website, with only a
few pages in `content/` until you get the HTML, CSS, and JavaScript right. Once you're happy with your theme, you'll
quite possibly only ever add new pages in `content/` as you fill out your website material.

## Why Nebulous CMS? ##

Over time I've tried many different static website builders (and even written a few) and I've written many dynamic
sites and flip-flopped between the two depending on how I feel and what I needed.

I've also created semi-static packages for Express which could serve up a directory of content or a directory of blog
posts. After writing most of a static generator a few years ago, I finally merged all of these ideas to create Nebulous
CMS.

After having [written most of it in that first week](/history) I realised that all of the problems I'd had with my
previous attempts were gone, the data structures created were nice, the code was **very** small, and the
`nebulous-core`, `nebulous-server` and `nebulous-cli` packages all complimented and interacted with each other quite
nicely. The plugin system was already starting to be fleshed out and at that point, with all of these things working so
nicely so quickly, I realised I was onto a good thing and decided to release it all as open source.

## Thanks ##

Thanks to the following people:

* [TJHolowayhuck](https://twitter.com/tjholowaychuk) for starting Express/Jade
* [Doug Wilson](https://somethingdoug.com/) for maintaining Express
* [Forbes Lindesay](http://www.forbeslindesay.co.uk/) for maintaining PugJS
* everyone who has ever done anything with Markdown or JSON
* [TJHolowayhuck](https://twitter.com/tjholowaychuk) for the [static](https://github.com/apex/static) theme we've commandeered for this site

(Ends)
