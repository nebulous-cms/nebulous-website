Nebulous started out of the blocks pretty quickly.

On Tue 15th May 2018 at around 11pm I created the "Nebulous CMS" project, and the start of the `nebulous-core`,
`nebulous-server`, `nebulous-cli` and `nebulous-website` repos.

By Wednesday, most of `nebulous-core` was done.

By Thursday, most of `nebulous-server` was done. I registered `nebulous.design` for what I knew would become my new
company.

By Friday I'd created a test website [palmy.org.nz](https://palmy.org.nz) which was fully functioning.

And by the time I'd gone to bed I'd created the new company's website too :
[nebulous.design](https://nebulous.design/).

By the end of the weekend, more features had been added to `nebulous-core` and `nebulous-server`, and by the following
Tuesday the [Nebulous CMS](https://nebulous-cms.org/) was also born.

Another week after and all four projects were at v1, with multiple example sites being hosted on heroku.com,
glitch.com, and now.sh.
